# X4 Cursor Colour Variation

Converts .CUR cursor files from one hue to another

Depends on
- coreutils (8.30) <https://www.gnu.org/software/coreutils/>
- sed (4.5) <https://www.gnu.org/software/sed/>
- ImageMagick (7.0.8-67) <https://imagemagick.org/>
- icoutils (0.32.2) <https://www.nongnu.org/icoutils/>
ImageMagick can read and write .CUR/.ICO files, but I couldn't find a way to
set the hotspot. One could easily patch the few required bytes manually...
<https://en.wikipedia.org/wiki/.cur>

Motivation:
- Improve user experience for the game "X4: Foundations" (pale grey
cursors on a mostly black background may sound cool, but is not what 
I'd call the cusp of usability)
- Converting the dozen or so files by hand would not have taken half
the time it took to cobble together this script, but this way one can
simply adjust a few parameters to get different colours, or even
different sizes.
- One could also easily generate a few sets, and mix-and-match.
- And last but not least, it generates a set of left-handed cursors.

The cursor files are located in {X4installdir}/game/mousecursors
Copy the entire directory to a safe workspace, then drop this script
in and run it from there.

Ideas for improvement:
- Pass HUE, SAT, and BRT as command line arguments
- Same goes for the output directory, and possibly input, too
- CUR files /may/ contain multiple layers; the current X4 cursors
don't, but it might be nice to catch and handle this case, too