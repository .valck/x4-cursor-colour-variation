#!/bin/env bash
# this is "cur_convert.sh"
# Converts .CUR cursor files from one hue to another

# Depends on
# - coreutils (8.30) <https://www.gnu.org/software/coreutils/>
# - sed (4.5) <https://www.gnu.org/software/sed/>
# - ImageMagick (7.0.8-67) <https://imagemagick.org/>
# - icoutils (0.32.2) <https://www.nongnu.org/icoutils/>
# ImageMagick can read and write .CUR/.ICO files, but I couldn't find a way to
# set the hotspot. One could easily patch the few required bytes manually...
# <https://en.wikipedia.org/wiki/.cur>

# Motivation:
# - Provide better visibility for the game X4 (pale grey cursors on
# a mostly black background may sound cool, but is not what I'd 
# call the cusp of usability)
# - Converting the dozen or so files by hand would not have taken half
# the time it took to cobble together this script, but this way one can
# simply adjust a few parameters to get different colours, or even
# different sizes.
# - One could also easily generate a few sets, and mix-and-match.
# - And last but not least, it generates a set of left-handed cursors.

# The cursor files are located in {X4installdir}/game/mousecursors
# Copy the entire directory to a safe workspace, then drop this script
# in and run it from there.

# Ideas for improvement:
# - Pass HUE, SAT, and BRT as command line arguments
# - Same goes for the output directory, and possibly input, too
# - CUR files /may/ contain multiple layers; the current X4 cursors
# don't, but it might be nice to catch and handle this case, too



# Initial experiments:
# $ icotool --list cursor.cur
# --cursor --index=1 --width=32 --height=32 --bit-depth=32 --palette-size=0 --hotspot-x=11 --hotspot-y=6
# $ icotool --extract cursor.cur
# <Use Gimp to adjust hue-chroma H+180 C+50, changing the cursor from grey to orange>
# $ icotool -c --cursor -i1 -w32 -h32 -b32 -p0 -X11 -Y 6 -o cursor.cur cursor_1_32x32x32.png
# $ icotool --list cursor.cur
# --cursor --index=1 --width=32 --height=32 --bit-depth=32 --palette-size=0 --hotspot-x=11 --hotspot-y=6
# $ mkdir output; for i in *.cur; do icotool --extract $i -o output/; done
# create all possible colour variations for reference:
# $ mkdir output; icotool -x -o output/cursor.png cursor.cur && for i in $(seq -w 0 1 200); do magick convert output/cursor.png -modulate 100,300,$i output/cursor_hue-$i.png ; done

# Now let's automate this

DIR=customcursors  # where to put the new set
DO_LEFT=true       # generate left-handed cursor set?

# Set the desired output options
# The HUE parameter ranges from 0..200, with 100 being "100% of PI",
# and 200 once around the entire circle of colours, same hue as 0.
# SAT and BRT are given as percentages of variation, they
# can go above 100 for the obvious reason:
# A SAT or BRT value of 100% means no change.
HUE=193  # orange-y; experiment to find *your* preferred colour
SAT=300  # more vibrant
BRT=100  # same brightness



if [[ true == ${DO_LEFT} ]] && [[ ! -d ${DIR}/left ]]; then
  mkdir -p ${DIR}/left || exit 1
elif [[ ! -d ${DIR} ]]; then
  mkdir -p ${DIR} || exit 1
fi

# from all *.cur files, remove the extension from the filename,
# and iterate over the resulting list
for ITM in $(basename -s .cur *.cur); do

  CUR=${ITM}.cur
  TMP=${DIR}/${ITM}-tmp.png
  PNG=${DIR}/${ITM}.png
  OUT=${DIR}/${ITM}.cur
  LPNG=${DIR}/left/${ITM}.png
  LOUT=${DIR}/left/${ITM}.cur

  # special treatment of "unavailable.cur" and "targetred.cur"
  if [[ "unavailable" = "${ITM}" ]] || [[ "targetred" = "${ITM}" ]]; then
    cp ${CUR} ${OUT}
    if [[ true == ${DO_LEFT} ]]; then
      cp ${CUR} ${LOUT}
    fi
    continue
  fi
  

  # convert to PNG
  icotool --extract ${CUR} -o ${TMP}
  # this way, we get PNG files with the same names as the
  # original CUR files

  # extract cursor metadata
  OPT=$(icotool --list ${CUR})

  # colour-adjust the PNG file:
  magick convert ${TMP} -modulate ${BRT},${SAT},${HUE} ${PNG}
  # and convert back to CUR format, using original metadata:
  icotool --create ${OPT} --output=${OUT} ${PNG}

  if [[ true == ${DO_LEFT} ]]; then
    # mirroring for left-handed cursors:
    magick convert -flop ${PNG} ${LPNG}
    # extract hotspot x/y coordinates and substract from width (and height if flipped vertically):
    CUR_W=$(echo ${OPT} | sed -e '{s/^.*width=//}' | sed -e '{s/ .*//}')
    CUR_H=$(echo ${OPT} | sed -e '{s/^.*height=//}' | sed -e '{s/ .*//}')
    HOT_X=$(echo ${OPT} | sed -e '{s/^.*spot-x=//}' | sed -e '{s/ .*//}')
    HOT_Y=$(echo ${OPT} | sed -e '{s/^.*spot-y=//}' | sed -e '{s/ .*//}')
    # adjust arguments and create CUR (using a -1 offset because W=32 means coordinates 0..31):
    OPT=$(echo "--cursor --index=1 --bit-depth=32 --palette-size=0 --width=${CUR_W} --height=${CUR_H} --hotspot-x=$((${CUR_W}-1-${HOT_X})) --hotspot-y=${HOT_Y}")
    icotool --create ${OPT} --output=${LOUT} ${LPNG}
  fi


  # clean up
  rm ${TMP} ${PNG}
  if [[ true == ${DO_LEFT} ]]; then
    rm ${LPNG}
  fi

done
